# docker-debian-stretch
Welcome to my repository for using my Docker images based on Debian Stretch.  These Docker images are stored on [Docker Hub](https://hub.docker.com/r/jhsu802701/).  These Docker images are customized for specific projects

## Prerequisites
Go to https://gitlab.com/rubyonracetracks/docker-debian-stretch.  The same prerequisites, warnings, and procedures that apply there also apply here.

### Images Available
* rails-codetriage: for [CodeTriage](https://github.com/jhsu802701/codetriage)
* rails-octobox: for [Octobox](https://github.com/jhsu802701/octobox)
* rails-rubymn2: for [RubyMN](https://github.com/jhsu802701/rubymn2)
* rails-sessionizer: For [Minnestar's Sessionizer app](https://github.com/jhsu802701/sessionizer)

## Building Docker Images
The scripts for building these Docker images are at https://bitbucket.org/jhsu802701/docker-debian-stretch-build.
